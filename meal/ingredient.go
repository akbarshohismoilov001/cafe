package meal

import "time"

type Ingredient struct {
	ID int
	Name string
	AddedAt time.Time
}