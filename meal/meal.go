package meal

type Meal struct {
	ID int
	Name string
	ReceptID int
	CategoryID int
}

func New(name string) Meal{
	return Meal{
		Name: name,
	}
}