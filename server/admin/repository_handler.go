package admin

import (
	"cafe/meal"
	"context"
)


type Admin interface {
	CafeBudget(c context.Context, id string) (int, error)
	AddInventory(c context.Context, n string, q int) (error)
	NewIngredient(c context.Context, n string, q int, p int) (error)
	InventoryStatus(c context.Context) ([]meal.Ingredient, error)
}


