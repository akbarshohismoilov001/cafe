package server

import (
	"cafe/meal"
	"context"
)

type RepositoryUser interface {
	Log(ctx context.Context, e string) (error)
	Budget(ctx context.Context, uID int) (int, error)
	Pay(ctx context.Context, id int) (error)
	Menu(ctx context.Context) []meal.Meal
	Ingredients(ctx context.Context, mID int) ([]meal.Ingredient, error)
	Ingredient(ctx context.Context, iID int) (meal.Ingredient, error)
	Choose(ctx context.Context, menu meal.Meal, num int) (error)
	AddMoney(ctx context.Context, cID int) (error)
	Feature(ctx context.Context, b int) ([]meal.Meal, error)
}

