package user

import "time"

type User struct {
	ID int
	Fullname string
	CardID int
	JoinedAt time.Time
	LogNumber int
}

func New() *User{
	return &User{}
}