package order

type Table struct {
	ID int
	UserID int
	Status bool
	Budget int
}