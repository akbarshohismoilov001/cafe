package order

type Order struct {
	ID int
	MealID int
	UserID int
	TableID int
}