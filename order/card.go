package order

type Card struct {
	ID int
	TableID int
	Budget int
}

func NewCard() *Card {
	return &Card{}
}