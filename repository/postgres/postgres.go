package postgres

import (
	"context"
	"database/sql"
	"time"
)


type PostgresRepository struct {
	DB *sql.DB
}

func (p PostgresRepository) Log(ctx context.Context, e string) (error){
	c, cencal := withtimeout(ctx)
	defer cencal()
	id, err := getUser(e, p.DB); if err != nil{
		return err
	}; if id != 0{
		p.DB.ExecContext(
			c, "update users set logs_num = logs_num + 1 where email=$1",e,
		)
		return nil 
	}
	p.DB.ExecContext(
		c, "insert into users values($1)",e,
	)
	return nil
}

func (p PostgresRepository) Budget(ctx context.Context, id int) (int, error) {
	c, cencal := withtimeout(ctx)
	defer cencal()
	var b int
	row := p.DB.QueryRowContext(
		c, "select budget from users where id=$1", id,
	); if err := row.Scan(&b); err != nil{
		return 0, err
	}
	return b, nil
}

func (p PostgresRepository) Pay(ctx context.Context, id int) error{
	c, cencal := withtimeout(ctx)
	defer cencal()

	tx, err := p.DB.BeginTx(c, &sql.TxOptions{}); if err != nil{
		return err
	};
	
	_, err = tx.ExecContext(
		c, "update cards set budget=budget-(select price from orders where id=$1) where user_id=$2",id,id,
	); if err != nil{
		return tx.Rollback()
	}
	_, err = tx.ExecContext(
		c, "update tables set budget=budget+(select price from orders where id=$1) where id=(select table_id from cards where id=$1",id,id,
	); if err != nil{
		return tx.Rollback()
	}
	_, err = tx.ExecContext(
		c, "update cafes set budget=budget+(select price from orders where id=$1)",id,
	); if err != nil{
		return tx.Rollback()
	}
	return nil
}

func getUser(e string, db *sql.DB) (int , error){
	var id int
	row := db.QueryRow(
		"select id from users where email=$1", e,
	)
	if err := row.Scan(&id); err != nil{
		return 0, err
	}
	return id, nil
}

func withtimeout (parent context.Context) (context.Context, context.CancelFunc){
	c, cencal := context.WithTimeout(parent, time.Millisecond * 2)
	return c, cencal
}