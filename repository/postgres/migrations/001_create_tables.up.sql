CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    email VARCHAR(50) UNIQUE,
    joined_at DATE DEFAULT CURRENT_DATE,
    logs_num INT DEFAULT 0
);


CREATE TABLE cafes (
    id SERIAL PRIMARY KEY,
    name VARCHAR(30) NOT NULL,
    table_num INT,
    budget INT
);


CREATE TABLE cards (
    id SERIAL PRIMARY KEY,
    budget INT DEFAULT 0,
    user_id INT REFERENCES users(id) ON DELETE CASCADE
);


CREATE TABLE tables (
    id SERIAL PRIMARY KEY,
    user_id INT REFERENCES users(id) ON DELETE CASCADE,
    cafe_id INT REFERENCES cafes(id) ON DELETE CASCADE,
    budget int DEFAULT 0,
    status BOOLEAN DEFAULT false
);


CREATE TABLE orders (
    id SERIAL PRIMARY KEY,
    table_id INT REFERENCES tables(id),
    user_id INT REFERENCES users(id) ON DELETE CASCADE,
    status BOOLEAN DEFAULT false,
    price INT
);


CREATE TABLE categories (
    id SERIAL PRIMARY KEY,
    name VARCHAR(20)
);


CREATE TABLE meals (
    id SERIAL PRIMARY KEY,
    name VARCHAR(20),
    category_id INT REFERENCES categories(id) ON DELETE CASCADE,
    order_id INT REFERENCES orders(id) ON DELETE CASCADE
);


CREATE TABLE recepts (
    id SERIAL PRIMARY KEY,
    meal_id INT REFERENCES meals(id) ON DELETE CASCADE
);


CREATE TABLE inventories (
    id SERIAL PRIMARY KEY,
    name VARCHAR(20),
    cafe_id INT REFERENCES cafes(id) ON DELETE CASCADE
);


CREATE TABLE ingredients (
    id SERIAL PRIMARY KEY,
    name VARCHAR(20),
    recept_id INT REFERENCES recepts(id) ON DELETE CASCADE,
    inventory_id INT REFERENCES inventories(id) ON DELETE CASCADE,
    qauntity INT,
    added_at DATE DEFAULT CURRENT_DATE 
);


CREATE TABLE cheffs (
    id SERIAL PRIMARY KEY,
    password VARCHAR(8) NOT NULL
);


CREATE TABLE admins (
    id SERIAL PRIMARY KEY,
    password VARCHAR(8) NOT NULL
);