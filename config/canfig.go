package config

import (
	
	_ "github.com/joho/godotenv/autoload"
)

type Config struct {
	Host             string
	Port             string
	PostgresHost     string
	PostgresPort     string
	PostgresUser     string
	PostgresPassword string
	PostgresDB       string
}

func Load() Config {
	c := Config{}

	c.Host = "localhost"
	c.Port =  "3333"
	c.PostgresHost =  "localhost"
	c.PostgresPort =  "5432"
	c.PostgresUser =  "akbarshoh"
	c.PostgresPassword =  "1"
	c.PostgresDB =  "cafe"
	return c
}

